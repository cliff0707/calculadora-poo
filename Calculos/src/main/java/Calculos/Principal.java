/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Calculos;

import javax.swing.JOptionPane;

/**
 *
 * @author asbar
 */
public class Principal {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Calculos calcular = new Calculos();
        String menu;
        boolean continuar = true;
        int numero = 0, numero2 = 0;

        do {
            numero = 0;
            numero2 = 0;
            calcular.setResultado(1);
            
            menu = JOptionPane.showInputDialog(
                    "Digite la opción  \n"
                    + "1. Suma \n"
                    + "2. Resta \n"
                    + "3. Multiplicación \n"
                    + "4. División \n"
                    + "5. Salir \n"
            );
            
            switch(menu){
                case "1":
                    
                    do {
                        numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número"));
                        calcular.suma(numero);
                        JOptionPane.showMessageDialog(null, calcular.imprimirResultad());
                        continuar = JOptionPane.showConfirmDialog(null, "Desea ingresar otro número?") == 0;   
                    } while (continuar);
                    
                    break;
                case "2":
                    do {
                        numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número"));
                        calcular.resta(numero);
                        JOptionPane.showMessageDialog(null, calcular.imprimirResultad());
                        continuar = JOptionPane.showConfirmDialog(null, "Desea ingresar otro número?") == 0;   
                    } while (continuar);
                    break;
                case "3":
                    do {
                        numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número"));
                        calcular.multiplicar(numero);
                        JOptionPane.showMessageDialog(null, calcular.imprimirResultad());
                        continuar = JOptionPane.showConfirmDialog(null, "Desea ingresar otro número?") == 0;   
                    } while (continuar);
                    break;
                case "4":
                    do {
                        numero = Integer.parseInt(JOptionPane.showInputDialog("Ingrese un número"));
                        numero2 = Integer.parseInt(JOptionPane.showInputDialog("Ingrese otro número"));
                        calcular.dividir(numero, numero2);
                        JOptionPane.showMessageDialog(null, calcular.imprimirResultad());
                        continuar = JOptionPane.showConfirmDialog(null, "Desea ingresar otro número?") == 0;   
                    } while (continuar);
                    break;
            }

        } while (!"5".equals(menu));
    }
    
}


    

